package mx.ms.eni.emergency_contact.business;

import mx.ms.eni.emergency_contact.model.dto.EmergencyContactDTO;

public interface IEmergencyContactService {

	EmergencyContactDTO getEmeContactsByIdUser(Integer idUser);

	void createUser(EmergencyContactDTO emeContactDTO);
	
	void updateUser(Integer idEmergencyContact, EmergencyContactDTO emeContactDTO);
	
	void deleteUser(Integer idEmergencyContact);
}
