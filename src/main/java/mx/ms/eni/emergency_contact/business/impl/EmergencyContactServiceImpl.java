package mx.ms.eni.emergency_contact.business.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.ms.eni.emergency_contact.business.IEmergencyContactService;
import mx.ms.eni.emergency_contact.model.dto.EmergencyContactDTO;
import mx.ms.eni.emergency_contact.model.entity.EmergencyContactEntity;
import mx.ms.eni.emergency_contact.model.repository.IEmergencyContactRepository;

@Service
public class EmergencyContactServiceImpl implements IEmergencyContactService {

	@Autowired
	IEmergencyContactRepository emeContactRepository = null;

	@Autowired
	EmergencyContactDTO emeContactDTO = null;

	public EmergencyContactDTO getEmeContactsByIdUser(Integer idUser) {
		Optional<EmergencyContactEntity> emeContactEntity = emeContactRepository.findByIdUser(idUser);
		if (emeContactEntity != null && emeContactEntity.get() != null) {
			emeContactDTO = new EmergencyContactDTO();
			emeContactDTO.setId(emeContactEntity.get().getId());
			emeContactDTO.setIdUser(emeContactEntity.get().getIdUser());
			emeContactDTO.setName(emeContactEntity.get().getName());
			emeContactDTO.setSurname(emeContactEntity.get().getSurname());
			emeContactDTO.setLastname(emeContactEntity.get().getLastname());
			emeContactDTO.setKinship(emeContactEntity.get().getKinship());
			emeContactDTO.setPhone(emeContactEntity.get().getPhone());
			emeContactDTO.setName2(emeContactEntity.get().getName2());
			emeContactDTO.setSurname2(emeContactEntity.get().getSurname2());
			emeContactDTO.setLastname2(emeContactEntity.get().getLastname2());
			emeContactDTO.setKinship2(emeContactEntity.get().getKinship2());
			emeContactDTO.setPhone2(emeContactEntity.get().getPhone2());
		}
		return emeContactDTO;
	}

	public void createUser(EmergencyContactDTO emeContactDTO) {
		EmergencyContactEntity emeContactEntity = new EmergencyContactEntity();
		emeContactEntity.setId(emeContactDTO.getId());
		emeContactEntity.setIdUser(emeContactDTO.getIdUser());
		emeContactEntity.setName(emeContactDTO.getName());
		emeContactEntity.setSurname(emeContactDTO.getSurname());
		emeContactEntity.setLastname(emeContactDTO.getLastname());
		emeContactEntity.setKinship(emeContactDTO.getKinship());
		emeContactEntity.setPhone(emeContactDTO.getPhone());
		emeContactEntity.setName2(emeContactDTO.getName2());
		emeContactEntity.setSurname2(emeContactDTO.getSurname2());
		emeContactEntity.setLastname2(emeContactDTO.getLastname2());
		emeContactEntity.setKinship2(emeContactDTO.getKinship2());
		emeContactEntity.setPhone2(emeContactDTO.getPhone2());
		emeContactRepository.save(emeContactEntity);
	}

	public void updateUser(Integer idEmergencyContact, EmergencyContactDTO emeContactDTO) {
		EmergencyContactEntity emeContactEntity = new EmergencyContactEntity();
		emeContactEntity.setIdUser(emeContactDTO.getIdUser());
		emeContactEntity.setName(emeContactDTO.getName());
		emeContactEntity.setSurname(emeContactDTO.getSurname());
		emeContactEntity.setLastname(emeContactDTO.getLastname());
		emeContactEntity.setKinship(emeContactDTO.getKinship());
		emeContactEntity.setPhone(emeContactDTO.getPhone());
		emeContactEntity.setName2(emeContactDTO.getName2());
		emeContactEntity.setSurname2(emeContactDTO.getSurname2());
		emeContactEntity.setLastname2(emeContactDTO.getLastname2());
		emeContactEntity.setKinship2(emeContactDTO.getKinship2());
		emeContactEntity.setPhone2(emeContactDTO.getPhone2());
		emeContactRepository.save(emeContactEntity);
	}

	public void deleteUser(Integer idEmergencyContact) {
		emeContactRepository.deleteById(idEmergencyContact);
	}
}
