package mx.ms.eni.emergency_contact.service;

import mx.ms.eni.emergency_contact.model.dto.EmergencyContactDTO;

public interface IEmergencyContactPublicService {
	
	EmergencyContactDTO getEmeContactsByIdUser(Integer idUser);

	void createUser(EmergencyContactDTO emeContactDTO);
	
	void updateUser(Integer idEmergencyContact, EmergencyContactDTO emeContactDTO);
	
	void deleteUser(Integer idEmergencyContact);
}
