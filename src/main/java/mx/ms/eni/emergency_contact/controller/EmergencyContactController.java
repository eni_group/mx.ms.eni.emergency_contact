package mx.ms.eni.emergency_contact.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.ms.eni.emergency_contact.business.IEmergencyContactService;
import mx.ms.eni.emergency_contact.model.dto.EmergencyContactDTO;
import mx.ms.eni.emergency_contact.service.IEmergencyContactPublicService;

@RestController
@RequestMapping(value="/emergency-contact")
public class EmergencyContactController implements IEmergencyContactPublicService{

	@Autowired
	IEmergencyContactService emeContactService = null;
	
	@Override
	@CrossOrigin(origins="*")
	@GetMapping(value="/", produces="application/json")
	public EmergencyContactDTO getEmeContactsByIdUser(@RequestParam Integer idUser)
	{
		return emeContactService.getEmeContactsByIdUser(idUser);
	}
		
	@Override
	@CrossOrigin(origins="*")
	@PostMapping(value="/", consumes = "application/json", produces="application/json")
	public void createUser(@RequestBody EmergencyContactDTO emeContactDTO)
	{
		emeContactService.createUser(emeContactDTO);
	}
	
	@Override
	@CrossOrigin(origins="*")
	@PutMapping(value="/", consumes = "application/json", produces="application/json")
	public void updateUser(@RequestParam Integer idEmeContact, @RequestBody EmergencyContactDTO emeContactDTO)
	{
		emeContactService.updateUser(idEmeContact, emeContactDTO);
	}
	
	@Override
	@DeleteMapping(value="/", consumes = "application/json", produces="application/json")
	public void deleteUser(@RequestParam Integer idEmeContact)
	{
		emeContactService.deleteUser(idEmeContact);
	}
}
