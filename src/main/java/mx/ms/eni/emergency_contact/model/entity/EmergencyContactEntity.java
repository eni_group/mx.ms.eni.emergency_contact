package mx.ms.eni.emergency_contact.model.entity;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="T_USER")
public class EmergencyContactEntity implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="idContactoEmergencia")
	private Integer id;
	
	@Column(name="idUsuario")
	private Integer idUser;
	
	@Column(name="Nombre", nullable = false)
	private String name;
	
	@Column(name="ApellidoPaterno", nullable = false)
	private String surname;
	
	@Column(name="ApellidoMaterno", nullable = false)
	private String lastname;
	
	@Column(name="Parentesco", nullable = false)
	private String kinship;
	
	@Column(name="Telefono", nullable = false)
	private String phone;

	@Column(name="Nombre2", nullable = false)
	private String name2;
	
	@Column(name="ApellidoPaterno2", nullable = false)
	private String surname2;
	
	@Column(name="ApellidoMaterno2", nullable = false)
	private String lastname2;
	
	@Column(name="Parentesco2", nullable = false)
	private String kinship2;
	
	@Column(name="Telefono2", nullable = false)
	private String phone2;
	
	public EmergencyContactEntity() {
		
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getKinship() {
		return kinship;
	}

	public void setKinship(String kinship) {
		this.kinship = kinship;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getSurname2() {
		return surname2;
	}

	public void setSurname2(String surname2) {
		this.surname2 = surname2;
	}

	public String getLastname2() {
		return lastname2;
	}

	public void setLastname2(String lastname2) {
		this.lastname2 = lastname2;
	}

	public String getKinship2() {
		return kinship2;
	}

	public void setKinship2(String kinship2) {
		this.kinship2 = kinship2;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	@Override
	public String toString() {
		return "EmergencyContactEntity [id=" + id + ", idUser=" + idUser + ", name=" + name + ", surname=" + surname
				+ ", lastname=" + lastname + ", kinship=" + kinship + ", phone=" + phone + ", name2=" + name2
				+ ", surname2=" + surname2 + ", lastname2=" + lastname2 + ", kinship2=" + kinship2 + ", phone2="
				+ phone2 + "]";
	}	
}
