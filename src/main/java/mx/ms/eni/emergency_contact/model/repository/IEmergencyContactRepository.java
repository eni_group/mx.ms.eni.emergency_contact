package mx.ms.eni.emergency_contact.model.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import mx.ms.eni.emergency_contact.model.entity.EmergencyContactEntity;

@Repository
public interface IEmergencyContactRepository extends JpaRepository<EmergencyContactEntity, Integer>{

	Optional<EmergencyContactEntity> findByIdUser(Integer idUser);
}
